import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
  import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IStudents } from 'src/app/model/istudents';
import { StudentServiceService } from 'src/app/shared/student-service.service';
  
@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
    hideForm:boolean=false
    buttons:string='Add new'
    registerForm!: FormGroup;
    students_info!: IStudents
    submitted = false;
    constructor(private formBuilder: FormBuilder , private _service_student : StudentServiceService) { }
    ngOnInit(): void {
      this.registerForm = this.formBuilder.group({
          name: ['', Validators.required],
          gender: ['', Validators.required],
          subject: ['', [Validators.required]]
      });
  }
  
  
  get f() { return this.registerForm.controls; }
  
  onSubmit() {
      this.students_info=this.registerForm.value
      this.submitted = true;
      if (this.registerForm.invalid) {
          return;
      }
      this._service_student.studentObs$.subscribe((students : IStudents[] )=>{
        students.push(this.students_info)
      })
      
  }
  buttonChange() {
    if(this.hideForm){
      this.hideForm = false
      this.buttons = "Add New"
    }
    else{
      this.hideForm = true
      this.buttons = "Hide"
    }
  }
  }
  