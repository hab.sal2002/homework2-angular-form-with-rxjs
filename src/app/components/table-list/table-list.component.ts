import { Component, OnInit } from '@angular/core';
import { IStudents } from 'src/app/model/istudents';
import { StudentServiceService } from 'src/app/shared/student-service.service';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  students_info!: IStudents[]
  constructor(private _student_list: StudentServiceService) { }
 
  ngOnInit(): void {
    this._student_list.studentObs$.subscribe((students: IStudents[])=>{
      this.students_info =students
    })
  }
  delete(id:any){
    this.students_info.splice(id,1)
  }


}
