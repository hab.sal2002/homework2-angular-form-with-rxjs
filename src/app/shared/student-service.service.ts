 
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IStudents } from '../model/istudents';

@Injectable({
  providedIn: 'root'
})
export class StudentServiceService {
  studentObs$ !: Observable<IStudents[]>

  students : IStudents[] = [
    {
      name: 'Mara Ona',
      gender: 'Male',
      subject: 'Spring'
    },
    {
      name: 'Kan Kanika',
      gender: 'Male',
      subject: 'Blockchain'
    },
    {
      name: 'Su Somany',
      gender: 'Male',
      subject: 'IOS'
    }
  ]

  constructor() { 
    this.studentObs$ = of(this.students)
  }
}

 